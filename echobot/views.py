# line_echobot/echobot/views.py

# WebhookHandler version


from datetime import datetime
from time import gmtime, strftime

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseForbidden)
from django.views.decorators.csrf import csrf_exempt
from linebot import LineBotApi, WebhookHandler, models
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import *
from datetime import timedelta
from .models import *

line_bot_api = LineBotApi(settings.LINE_CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(settings.LINE_CHANNEL_SECRET)

#==========TEMPLATE BUAT BIKIN CONTENT===============

#Template buat postback

def postback(labels,datas):
    return PostbackAction(label=labels,data=datas)

#Template buat carousel dengan image
def Carousel(imageURL, titles, texts, action ): 
    return CarouselColumn(thumbnail_image_url=imageURL,
                        title=titles,
                        text=texts,
                        actions=action  #Action berisi postback
                    )

def CarouselWithoutImage( titles, texts, action ): 
    return ButtonsTemplate(title=titles,
                        text=texts,
                        actions=action  #Action berisi postback
                    )
#Template buat bikin tampilan cafe
def cafe(imageURL,name, adress, time, linkGmaps ):
    return BubbleContainer(
                direction='ltr',
                hero=ImageComponent(
                    url=imageURL,
                    size='full',
                    aspect_ratio='20:13',
                    aspect_mode='cover',
                    
                ),
                body=BoxComponent(
                    layout='vertical',
                    contents=[
                        # title
                        TextComponent(text=name,
                                    weight='bold', size='xl'),
            
                        # info
                        BoxComponent(
                            layout='vertical',
                            margin='lg',
                            spacing='sm',
                            contents=[
                                BoxComponent(
                                    layout='baseline',
                                    spacing='sm',
                                    contents=[
                                        TextComponent(
                                            text='Place',
                                            color='#aaaaaa',
                                            size='sm',
                                            flex=1
                                        ),
                                        TextComponent(
                                            text=adress,
                                            wrap=True,
                                            color='#666666',
                                            size='sm',
                                            flex=5
                                        )
                                    ],
                                ),
                                BoxComponent(
                                    layout='baseline',
                                    spacing='sm',
                                    contents=[
                                        TextComponent(
                                            text='Time',
                                            color='#aaaaaa',
                                            size='sm',
                                            flex=1
                                        ),
                                        TextComponent(
                                            text=time,
                                            wrap=True,
                                            color='#666666',
                                            size='sm',
                                            flex=5,
                                        ),
                                    ],
                                ),
                            ],
                        )
                    ],
                ),
                footer=BoxComponent(
                    layout='vertical',
                    spacing='sm',
                    contents=[
                        # callAction, separator, websiteAction
                        SpacerComponent(size='sm'),
                        # callAction
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color='#1ED056',
                            action=PostbackAction(label='Choose', data="Jumlah"),
                        ),
                        # separator
                        SeparatorComponent(),
                        # websiteAction
                        ButtonComponent(
                            style='link',
                            height='sm',
                            action=URIAction(label='Location', uri=linkGmaps)
                        )
                    ]
                ),
            )

#====================================================

@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):
    text = event.message.text
    print(text)
    print(event)

    if text == 'belajar':
        #Cek dulu dia udah terdaftar di database ato belom, cek dr USERID
        if isinstance(event.source, SourceUser):
            profile = line_bot_api.get_profile(event.source.user_id)
            user=Mentee.objects.filter(line_account_id=profile.user_id)
            
            if(user.count()==0):
                line_bot_api.reply_message(
                event.reply_token, [
                    TextSendMessage(text="Halo " + profile.display_name +
                                    ",\nSebelum nya kami membutuhkan nama beserta email kamu nih, balas dengan \n""/daftar <Nama Lengkap> <Email>"". \nContoh, /daftar Budi Budian@gmail.com"),
                    ]
                )
            else:
                Mentoring.objects.create(mentee=user[0])
                temp=list()
                for i in MataPelajaran.objects.all():
                    temp.append(Carousel(i.url_gambar,i.nama,"IPA",[postback("Pilih", "Matpel "+i.nama)]))
                Matpel = TemplateSendMessage(    #template data matpel
                    alt_text='Pilih mata pelajaran',
                    template=CarouselTemplate( 
                        columns=temp
                    )
                )
                line_bot_api.reply_message(
                event.reply_token, [
                    TextSendMessage(text="Halo " + user[0].nama+ ", kamu mau belajar mata pelajaran apa?" ),
                    Matpel
                ]
                )

            

        else:
            line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Bot can't use profile API without user ID"))

    #Pendaftaran
    elif text == 'reset':
        profile = line_bot_api.get_profile(event.source.user_id)
        Mentee.objects.all().filter(line_account_id=profile.user_id).delete()
        line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Menghapus memori.."))


    elif text.split(" ")[0] == '/daftar':
        profile = line_bot_api.get_profile(event.source.user_id)
        userObj=Mentee.objects.filter(line_account_id=profile.user_id)

        if(userObj.count()==0):
            user = (" ").join(text.split(" ")[1:-1])
            mail= text.split(" ")[-1]
            userId=profile = line_bot_api.get_profile(event.source.user_id).user_id
            try:
                validate_email(mail)
                
                userBaru= Mentee.objects.create(nama=user, line_account_id=userId, email=mail)
                Mentoring.objects.create(mentee=userBaru)
                Matpel = TemplateSendMessage(    #template data matpel
                        alt_text='Pilih mata pelajaran',
                        template=CarouselTemplate(
                            columns=[
                                
                                Carousel('https://i.ibb.co/6Zq6nW5/physics1.jpg',
                                        'Fisika',
                                        'IPA', 
                                        [postback("Pilih", "Matpel Fisika")]),
                                
                                Carousel('https://i.ibb.co/Hg5BcfY/DQma-B2px-Ws9ork-NVVJMo-TYh7ybh-VDCFu-Ar-Kzy9-R1x3v-Ccm-H.png',
                                        'Kimia',
                                        'IPA', 
                                        [postback("Pilih", "Matpel Kimia")]),
                                
                                Carousel('https://i.ibb.co/pbPtMcj/ekonomi.jpg',
                                        'Ekonomi',
                                        'IPS', 
                                        [postback("Pilih", "Matpel Ekonomi")]),
                            ]
                        )
                    )
                line_bot_api.reply_message(
                    event.reply_token, [
                        TextSendMessage(text="Selamat " + user + " kamu telah terdaftar sebagai pengguna Mentoria.id"),
                        TextSendMessage(text=user + ", kamu mau belajar mata pelajaran apa?"),
                        Matpel])
            except ValidationError:
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text=mail + " bukan Email yang valid"))
        
        else:
            line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text=userObj[0].nama + ', kamu telah terdaftar. Ketik "belajar" untuk melakukan pemesanan'))
                
        #=============Create model user===============

    #buat left grup
    elif text == 'bye':
        if isinstance(event.source, SourceGroup):
            line_bot_api.reply_message(
                event.reply_token, TextSendMessage(text='Dadah dikick ama ' + line_bot_api.get_profile(event.source.user_id).display_name))
            line_bot_api.leave_group(event.source.group_id)
        elif isinstance(event.source, SourceRoom):
            line_bot_api.reply_message(
                event.reply_token, TextSendMessage(text='Dadah dikick ama ' + line_bot_api.get_profile(event.source.user_id).display_name))
            line_bot_api.leave_room(event.source.room_id)
        else:
            line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Bot can't leave from 1:1 chat"))

    elif text == '.help':
        help_button= ButtonsTemplate(title="Butuh bantuan?" 
                                ,text="Temukan solusinya" ,
                                actions=[   PostbackAction(label='Cara pemesanan', data='Help cara'),
                                            PostbackAction(label='Setelah pemesanan', data='Help setelah'),
                                        ]
                                    )
        template_message = TemplateSendMessage(alt_text='Confirm alt text', template=help_button)
        line_bot_api.reply_message(event.reply_token, template_message)

        
    else:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="Anda perlu bantuan? balas "".help"" ")
        )


@handler.add(PostbackEvent)
def handle_postback(event):
    if event.postback.data.split(" ")[0] == 'Matpel':
        profile = line_bot_api.get_profile(event.source.user_id)
        matpel = event.postback.data.split(" ")[1]  #Dapetin data matpel yg udh dipilih sebelumnya
        mentee = Mentee.objects.filter(line_account_id=profile.user_id).first()
        edited_mentoring = Mentoring.objects.filter(mentee=mentee).filter(is_ordered=False).first()
        edited_mentoring.mata_pelajaran=MataPelajaran.objects.filter(nama=matpel).first()
        edited_mentoring.save()

        jenjang_carousel = TemplateSendMessage(    #template data matpel
            alt_text='Carousel template',
            template=CarouselWithoutImage(
                        "Jenjang Pelajaran",
                        "Pilih jenjang kamu",
                        [postback("Kelas 10", "Kelas 10"),
                        postback("Kelas 11", "Kelas 11"),
                        postback("Kelas 12", "Kelas 12"),
                        postback("SBMPTN", "Kelas SBMPTN"),
                    ]
                    )
        )
        line_bot_api.reply_message(
            event.reply_token, [
                TextSendMessage(text='Kamu memilih pelajaran ' + matpel),
                TextSendMessage(text='Selanjutnya, pilih jenjang kamu ya'),
                jenjang_carousel
            ])
        

    elif event.postback.data.split(" ")[0] == 'Kelas':
        profile = line_bot_api.get_profile(event.source.user_id)
        mentee = Mentee.objects.filter(line_account_id=profile.user_id).first()
        edited_mentoring = Mentoring.objects.filter(mentee=mentee).filter(is_ordered=False).first()
        template = ImageCarouselTemplate(columns=[
                                ImageCarouselColumn(image_url='https://i.ibb.co/NZZw9YF/adsdsdsad.png',
                                action=DatetimePickerAction(label='Click here',
                                                            data='datetime_postback',
                                                            mode='datetime')),
                                ])
        date_template = TemplateSendMessage(alt_text='ImageCarousel alt text', template=template)
        jenjang = event.postback.data  #Dapetin data matpel yg udh dipilih sebelumnya
        edited_mentoring.jenjang= Jenjang.objects.filter(nama=jenjang).first()
        edited_mentoring.save()
        line_bot_api.reply_message(
            event.reply_token, [TextSendMessage(text='Kamu memilih jenjang ' + jenjang),       #kasih info + kasih data daerah
                                TextSendMessage(text='Yuk pilih waktu belajar kamu'),
                                date_template
                                ])
    
    elif event.postback.data == 'datetime_postback':
        sekarang=strftime("%Y-%m-%d %H:%M:%S", gmtime()).split(" ")
        pilihan=event.postback.params['datetime'].split('T')
        tanggalSekarang= sekarang[0].split("-")
        jamSekarang= sekarang[1].split(":")
        tanggalPilihan= pilihan[0].split("-")
        jamPilihan= pilihan[1].split(":")
        d1=datetime(int(tanggalSekarang[0]), int(tanggalSekarang[1]),int(tanggalSekarang[2]),int(jamSekarang[0])+7,int(jamSekarang[1]))
        d2=datetime(int(tanggalPilihan[0]), int(tanggalPilihan[1]),int(tanggalPilihan[2]),int(jamPilihan[0]),int(jamPilihan[1]))
        print(d1)
        print(d2)
        profile = line_bot_api.get_profile(event.source.user_id)
        mentee = Mentee.objects.filter(line_account_id=profile.user_id).first()
        edited_mentoring = Mentoring.objects.filter(mentee=mentee).filter(is_ordered=False).first()
        date_template = ImageCarouselTemplate(columns=[
                                ImageCarouselColumn(image_url='https://i.ibb.co/NZZw9YF/adsdsdsad.png',
                                action=DatetimePickerAction(label='Click here',
                                                            data='datetime_postback',
                                                            mode='datetime')),
                                ])
        template_message = TemplateSendMessage(alt_text='ImageCarousel alt text', template=date_template)
        if(d1>d2):
            line_bot_api.reply_message(
            event.reply_token,[
            TextSendMessage(text="Waktu yang anda pilih sudah terlewat, silahkan pilih lagi"),
            template_message
            ]
            )
        else:
            if (d2-d1>timedelta(hours=12)):
                line_bot_api.reply_message(
                event.reply_token,[
                TextSendMessage(text="Waktu pemesanan terlalu dini, pastikan jam pemesanan mu kurang dari 12 jam dari sekarang"),
                template_message
                ])
            else:
                profile = line_bot_api.get_profile(event.source.user_id)
                edited_mentoring.tanggal=pilihan[0]
                edited_mentoring.waktu_mulai= pilihan[1]
                edited_mentoring.save()
                daerah_carousel = TemplateSendMessage(    #Template daftar daerah
                    alt_text='Daerah',
                    template=CarouselTemplate(
                        columns=[
                            Carousel('https://i.ibb.co/7kBQmyT/cerita-cafe1.jpg',
                                    'Jakarta Selatan',
                                    'Pilih daerah berikut', 
                                    [postback("Pasar Minggu", "Daerah Pasar Minggu"),
                                    postback("Kemang", "Daerah Kemang"), 
                                    postback("Tebet", "Daerah Tebet")]),

                            Carousel('https://i.ibb.co/nM0Z9bh/Pause.jpg',
                                    'Jakarta TImur',
                                    'Pilih daerah berikut',
                                    [postback("Kelapa Gading", "Daerah Kelapa Gading"),
                                    postback("Klender", "Daerah Klender"),
                                    postback("Pondok Bambu", "Daerah Pondok Bambu")]),
                            
                        ]
                    )
                )
                
                line_bot_api.reply_message(
                    event.reply_token, [
                        TextSendMessage(text='Kamu memilih tanggal ' + pilihan[0] + " pada jam "+ pilihan[1]),
                        daerah_carousel
                    ])








    elif event.postback.data.split(" ")[0] == 'Daerah':
        #buat data cafe
        Cafes= CarouselContainer(contents=[
            cafe('https://bloximages.chicago2.vip.townnews.com/newsandtribune.com/content/tncms/assets/v3/editorial/6/b0/6b07ade0-f250-11e7-92b0-a361d177d3ce/5a4fd4e67c860.image.jpg?resize=400%2C266',
            "Dunkin Donuts",
            "Jl.Tebet dalam No 42, Jakarta Selatan",
            "10.00-18.00",
            "https://www.google.com/maps/dir//Dunkin'+Donuts,+Jl.+KH.+Abdullah+Syafe'i+No.+11,+Tebet,+RT.13%2FRW.5,+Bukit+Duri,+Tebet,+Kota+Jakarta+Selatan,+Daerah+Khusus+Ibukota+Jakarta+12820/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x2e69f3853dd8486f:0x680fe0c80de8d4ef?ved=2ahUKEwio46jT6-XfAhVUWysKHeJrAGkQ48ADMAF6BAgAECk"
            ),
            cafe('https://bloximages.chicago2.vip.townnews.com/newsandtribune.com/content/tncms/assets/v3/editorial/6/b0/6b07ade0-f250-11e7-92b0-a361d177d3ce/5a4fd4e67c860.image.jpg?resize=400%2C266',
            "Puma",
            "Jl. deket ui",
            "10.00-18.00",
            "https://www.google.com/maps/dir//Dunkin'+Donuts,+Jl.+KH.+Abdullah+Syafe'i+No.+11,+Tebet,+RT.13%2FRW.5,+Bukit+Duri,+Tebet,+Kota+Jakarta+Selatan,+Daerah+Khusus+Ibukota+Jakarta+12820/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x2e69f3853dd8486f:0x680fe0c80de8d4ef?ved=2ahUKEwio46jT6-XfAhVUWysKHeJrAGkQ48ADMAF6BAgAECk"
            ),

            ]
        )
        message = FlexSendMessage(alt_text="hello", contents=Cafes)
        line_bot_api.reply_message(
            event.reply_token,
            message
        )
    elif event.postback.data == 'Jumlah':
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(
                text='Jumlah orang nya berapa?',
                quick_reply=QuickReply(
                    items=[
                        QuickReplyButton(
                            action=PostbackAction(label="1 Orang", data="Fix")
                        ),
                        QuickReplyButton(
                            action=PostbackAction(label="2 Orang", data="Fix")
                        ),
                        QuickReplyButton(
                            action=PostbackAction(label="3 Orang", data="Fix")
                        ),
                        QuickReplyButton(
                            action=PostbackAction(label="4 Orang", data="Fix")
                        ),
                        QuickReplyButton(
                            action=PostbackAction(label="5 Orang", data="Fix")
                        ),
                    ])))

    elif event.postback.data.split(" ")[0] == 'Fix':
        bubble = BubbleContainer(
                hero=ImageComponent(
                    url='https://d1nabgopwop1kh.cloudfront.net/v2/culinary-asset/4sh5mfy8I10NcCF4I8BR4vVv8wHkiioH1cHvruzN5QpRiGPt0N1m64ADRtwxS9wH8dBGbBMGrApw2+gFUBu1Fa5nol5CZzDkIDUlbzF5PY+HJheSXPJXSqxar6JL5ePMQ6YrOeAlXfvLHBdXZ1OaeA==',
                    size='full',
                    aspect_ratio='20:13',
                    aspect_mode='cover',
                    action=URIAction(uri='http://example.com', label='label')
                ),
                body=BoxComponent(
                    layout='vertical',
                    contents=[
                        # title
                        TextComponent(
                            text="Your Transaction", weight='bold', size='xl'),
                        # review
                        BoxComponent(
                            layout='vertical',
                            margin='lg',
                            spacing="sm",
                            contents=[
                                    BoxComponent(
                                        layout='baseline',
                                        spacing="sm",
                                        contents=[
                                            TextComponent(
                                                text="Date", color="#aaaaaa", size='sm', flex=1),
                                            TextComponent(
                                                text="Monday 25, 10.00 PM", color="#666666", size='sm', flex=4),
                                        ]
                                    ),
                                BoxComponent(
                                        layout='baseline',
                                        spacing="sm",
                                        contents=[
                                            TextComponent(
                                                text="Place", color="#aaaaaa", size='sm', flex=1),
                                            TextComponent(
                                                text="Jl.Tebet raya hehe", color="#666666", size='sm', flex=4),
                                        ]
                                        ),
                                BoxComponent(
                                        layout='baseline',
                                        spacing="sm",
                                        contents=[
                                            TextComponent(
                                                text="Price", color="#aaaaaa", size='sm', flex=1),
                                            TextComponent(
                                                text="IDR 100.000", color="#666666", size='sm', flex=4),
                                        ]
                                        )
                            ]
                        ),
                        BoxComponent(
                            layout="vertical",
                            margin="xxl",
                            contents=[
                                SpacerComponent(),
                                ImageComponent(
                                    url="https://mapstore2.readthedocs.io/en/user_docs/img/position.png",
                                    aspect_mode="cover",
                                    size="xl"
                                ),
                                ButtonComponent(
                                    style="primary",
                                    color="#1ED056",
                                    margin="xxl",
                                    height="sm",
                                    action=URIAction(
                                        uri='http://example.com', label='Set Reminder')
                                )
                            ]
                        )

                    ],
                )
            )
        #buat kasi konfirmasi 
        confirm_template = ConfirmTemplate(text='Proses Transaksi', actions=[
            postback("Iya", "confirm Yes"),
            postback("Tidak", "confirm No")
        ])
        Fixmessage = FlexSendMessage(alt_text="hello", contents=bubble)
        Confirmmessage = TemplateSendMessage(alt_text='Confirm alt text', template=confirm_template)
        line_bot_api.reply_message(
            event.reply_token,
            [Fixmessage,
            Confirmmessage]
            )
    elif event.postback.data.split(" ")[0] == 'confirm':
        profile = line_bot_api.get_profile(event.source.user_id)
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="Terima kasih "+ profile.display_name +", untuk melakukan komunikasi lebih lanjut dengan mentor yang kamu dapat, selanjutnya sliahkan masuk line square  di link berikut line.com/sqr249=12," )
        )
        


@csrf_exempt
def callback(request):
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')

        try:
            handler.handle(body, signature)
        except InvalidSignatureError:
            return HttpResponseForbidden()
        except LineBotApiError:
            return HttpResponseBadRequest()
        return HttpResponse()
    else:
        return HttpResponseBadRequest()
