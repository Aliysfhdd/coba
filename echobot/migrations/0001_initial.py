# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-17 02:20
from __future__ import unicode_literals

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HariMentoring',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('hari', models.CharField(max_length=10, unique=True, validators=[django.core.validators.MaxLengthValidator(10)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Jenjang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=15, unique=True, validators=[django.core.validators.MaxLengthValidator(15)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Kafe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=100, validators=[django.core.validators.MaxLengthValidator(100)])),
                ('url_google_maps', models.URLField()),
                ('waktu_buka', models.TimeField(default=datetime.time(10, 0), null=True)),
                ('waktu_tutup', models.TimeField(default=datetime.time(23, 0), null=True)),
                ('hari_mentoring_tersedia', models.ManyToManyField(related_name='kafe', to='echobot.HariMentoring')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LINESquareURL',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=50, validators=[django.core.validators.MaxLengthValidator(50)])),
                ('url', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MataPelajaran',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=20, unique=True, validators=[django.core.validators.MaxLengthValidator(20)])),
                ('line_group_id', models.CharField(max_length=100, null=True, validators=[django.core.validators.MaxLengthValidator(100)])),
                ('group_counter', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)])),
                ('url_gambar', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Mentee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=50, validators=[django.core.validators.MaxLengthValidator(50)])),
                ('line_account_id', models.CharField(max_length=100, unique=True, validators=[django.core.validators.MaxLengthValidator(100)])),
                ('email', models.EmailField(max_length=254)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Mentor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=50, validators=[django.core.validators.MaxLengthValidator(50)])),
                ('line_account_id', models.CharField(max_length=100, unique=True, validators=[django.core.validators.MaxLengthValidator(100)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Mentoring',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('tanggal', models.DateField(null=True)),
                ('waktu_mulai', models.TimeField(null=True)),
                ('waktu_selesai', models.TimeField(null=True)),
                ('biaya', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('jumlah_mentee', models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)])),
                ('is_ordered', models.BooleanField(default=False)),
                ('jenjang', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='mentoring', to='echobot.Jenjang')),
                ('kafe', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='mentoring', to='echobot.Kafe')),
                ('mata_pelajaran', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='mentoring', to='echobot.MataPelajaran')),
                ('mentee', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='mentoring', to='echobot.Mentee')),
                ('mentor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='mentoring', to='echobot.Mentor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nama', models.CharField(max_length=50, unique=True, validators=[django.core.validators.MaxLengthValidator(50)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='linesquareurl',
            name='mata_pelajaran',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='line_square_url', to='echobot.MataPelajaran'),
        ),
        migrations.AddField(
            model_name='kafe',
            name='region',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='kafe', to='echobot.Region'),
        ),
    ]
