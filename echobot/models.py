from datetime import time

from django.core.validators import MinValueValidator, MaxValueValidator, MaxLengthValidator
from django.db import models


class TimeStampModel(models.Model):
    """
    Abstract base class model that provides self-updating
    'created' and 'modified' fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Jenjang(TimeStampModel):
    nama = models.CharField(max_length=15, unique=True,
                            validators=[MaxLengthValidator(15)])

    def __str__(self):
        return self.nama


class Region(TimeStampModel):
    nama = models.CharField(max_length=50, unique=True,
                            validators=[MaxLengthValidator(50)])

    def __str__(self):
        return self.nama


class HariMentoring(TimeStampModel):
    hari = models.CharField(max_length=10, unique=True,
                            validators=[MaxLengthValidator(10)])

    def __str__(self):
        return self.hari


class Kafe(TimeStampModel):
    nama = models.CharField(max_length=100,
                            validators=[MaxLengthValidator(100)])
    region = models.ForeignKey(Region, on_delete=models.SET_NULL,
                               related_name='kafe', null=True)
    hari_mentoring_tersedia = models.ManyToManyField(HariMentoring, related_name='kafe')
    url_google_maps = models.URLField()
    waktu_buka = models.TimeField(null=True,
                                  default=time(hour=10, minute=0, second=0))
    waktu_tutup = models.TimeField(null=True,
                                   default=time(hour=23, minute=0, second=0))

    def __str__(self):
        return self.nama


class MataPelajaran(TimeStampModel):
    nama = models.CharField(max_length=20, unique=True,
                            validators=[MaxLengthValidator(20)])
    line_group_id = models.CharField(max_length=100, null=True,
                                     validators=[MaxLengthValidator(100)])
    group_counter = models.IntegerField(validators=[MinValueValidator(0)], default=0)
    url_gambar = models.URLField()

    def __str__(self):
        return self.nama


class LINESquareURL(TimeStampModel):
    nama = models.CharField(max_length=50,
                            validators=[MaxLengthValidator(50)])
    url = models.URLField()
    mata_pelajaran = models.ForeignKey(MataPelajaran, related_name='line_square_url',
                                       on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.nama


class Mentee(TimeStampModel):
    nama = models.CharField(max_length=50,
                            validators=[MaxLengthValidator(50)])
    line_account_id = models.CharField(max_length=100,
                                       validators=[MaxLengthValidator(100)],
                                       unique=True)
    email = models.EmailField()

    def __str__(self):
        return self.nama


class Mentor(TimeStampModel):
    nama = models.CharField(max_length=50,
                            validators=[MaxLengthValidator(50)])
    line_account_id = models.CharField(max_length=100,
                                       validators=[MaxLengthValidator(100)],
                                       unique=True)

    def __str__(self):
        return self.nama


class Mentoring(TimeStampModel):
    mentee = models.ForeignKey(Mentee, on_delete=models.SET_NULL,
                               related_name='mentoring', null=True)
    mentor = models.ForeignKey(Mentor, on_delete=models.SET_NULL,
                               related_name='mentoring', null=True)
    jenjang = models.ForeignKey(Jenjang, on_delete=models.SET_NULL,
                                related_name='mentoring', null=True)
    kafe = models.ForeignKey(Kafe, on_delete=models.SET_NULL,
                             related_name='mentoring', null=True)
    mata_pelajaran = models.ForeignKey(MataPelajaran, on_delete=models.SET_NULL,
                                       related_name='mentoring', null=True)
    tanggal = models.DateField(null=True)
    waktu_mulai = models.TimeField(null=True)
    waktu_selesai = models.TimeField(null=True)
    biaya = models.IntegerField(validators=[MinValueValidator(0)], null=True)
    jumlah_mentee = models.IntegerField(validators=[MinValueValidator(1),
                                                    MaxValueValidator(5)], null=True)
    is_ordered = models.BooleanField(default=False)
